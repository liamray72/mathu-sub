

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>


const uint64_t pipeOut = 0xE8E8F0F0E1LL;     //Remember that this code is the same as in the transmitter
RF24 radio(7, 8);  //CSN and CE pins

// The sizeof this struct should not exceed 32 bytes
struct Signal {
  byte throttle;
  byte left_right;
  byte up_down;  
};


Signal data;

/**************************************************/
void ResetData() 
{
data.throttle = 127; // Motor Stop (254/2=127)
data.left_right = 127; // Center 
data.up_down = 127; // Center 
}
void setup()
{
  //Once again, begin and radio configuration
  radio.begin();
  radio.openWritingPipe(pipeOut);
  radio.stopListening(); //start the radio comunication for Transmitter 
  ResetData();
}

/**************************************************/

int mapJoystickValues(int val, int lower, int middle, int upper, bool reverse)
{
val = constrain(val, lower, upper);
if ( val < middle )
val = map(val, lower, middle, 0, 128);
else
val = map(val, middle, upper, 128, 255);
return ( reverse ? 255 - val : val );
}

void loop()
{
// Control Stick Calibration | Kumanda Kol Kalibrasyonları
// Setting may be required for the correct values of the control levers. | Kolların doğru değerleri için ayar gerekebilir.
data.throttle = mapJoystickValues( analogRead(A0), 12, 524, 1020, true );
data.left_right = mapJoystickValues( analogRead(A1), 12, 524, 1020, true );      // "true" or "false" for servo direction 
data.up_down = mapJoystickValues( analogRead(A2), 12, 524, 1020, true );     // "true" or "false" for servo direction 
radio.write(&data, sizeof(Signal));
}
